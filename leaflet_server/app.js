const express = require('express')
const fs = require('fs');

const app = express()
const port = 3000

const pics = JSON.parse(fs.readFileSync('frontend/gpx_pics.json', 'utf8'));
const length = fs.readdirSync('frontend/data/').length

app.use(express.static("frontend",{ maxAge: 86400000 }))

app.get('/pics', (req, res) => {
    return res.send(Object.values(pics));
});

app.get('/total_files', (req, res) => {
    return res.send({'total_files':length});
});

app.listen(port, function(){
    console.log('Example app listening on port 3000!');
});
