var map = L.map('maps').setView([50.01480913139177, 8.255897459815287], 13);
const tiles = L.tileLayer('', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

const myCustomColour = 'red'

const markerHtmlStyles = `
  background-color: ${myCustomColour};
  width: 30px;
  height: 30px;
  display: block;
  position: relative;
  border-radius: 3rem 3rem 0;
  
  transform: rotate(45deg);
  border: 1px solid #FFFFFF`

const imageStyle = `
    transform: rotate(-45deg);
    font-size: 15px;
    color:red;
    border-radius: 50%;
    margin-left:5px;
    margin-top:5px;
    padding:2px 2px;
    background-color:white;
`
const circleStyle = `
    height: 10px;
    width: 10px;
    padding: 2.5px;
    border: solid 2.5px #c00;
    border-radius: 50%;
    background-color: #c00;
    background-clip: content-box;
`

const icon = L.divIcon({
  className: "my-custom-pin",
  iconSize: [30,30],
  iconAnchor: [15, 30],
  html: `<div style="${markerHtmlStyles}"><i class="fa fa-home" style="${imageStyle}"></i></div>`
})

const circleIcon = L.divIcon({
    className: "my-custom-circle",
    // iconSize: [30,30],
    iconAnchor: [5, 10],
    html: `<div style="${circleStyle}"></div>`
  })

L.marker([50.01480913139177, 8.255897459815287],{ icon:  icon}).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')

async function getJSON(file) {
    let x = await fetch(file);
    let img_coords = await x.json();

    let y = await fetch('/total_files');
    let files_lenght = await y.json();
    console.log(files_lenght['total_files'])

    let all_spots = {}

    Array(files_lenght['total_files']).fill(0).map((_, i) => {
        fetch('data/'+i+'.json')
        .then(response => response.json())
        .then(jsonResponse => {
            all_spots[i] = []
            jsonResponse.forEach((element, index) => {
                if (index %400 == 0){
                    all_spots[i].push([element['latitude'],element['longitude']])
                }
                try{
                    let latlngs = [
                        [element['latitude'], element['longitude']],
                        [jsonResponse[index+1]['latitude'], jsonResponse[index+1]['longitude']],
                    ];
                    var polyline = L.polyline(latlngs, {color: jsonResponse[index+1]['colors'],weight: 2}).addTo(map); 
                }
                catch (error) {
                    console.log('ended')
                }
            });
        })     
    });

    L.simpleMapScreenshoter().addTo(map)

    console.log(all_spots);
    let index_next_line = 0
    const nextLine = () => {
        let lat = Object.values(all_spots).flat()[index_next_line][0];
        let lon = Object.values(all_spots).flat()[index_next_line][1]
        map.setView(new L.LatLng(lat, lon),13);
        console.log("lat_lon: ",lat,lon)
        index_next_line+=1;
    }

    console.log(img_coords)
    img_coords.forEach((element) => {
        let photoImg = '<img style="object-fit:cover;" src="'+element['img']+'" height="150px" width="150px"/><p>' 
        // +element['img']+'</p>';
        let popupLocation1 = new L.LatLng(element['gps'][0], element['gps'][1]);
    
        let popupContent1 = photoImg;
        popup1 = new L.Popup({offset: element['off']
            // , closeButton: false
        });
    
        popup1.setLatLng(popupLocation1);
        popup1.setContent(popupContent1);
    
    
        map.addLayer(popup1);
        L.marker(element['gps'],{ icon:  circleIcon}).addTo(map)
    })
}


getJSON('/pics')

